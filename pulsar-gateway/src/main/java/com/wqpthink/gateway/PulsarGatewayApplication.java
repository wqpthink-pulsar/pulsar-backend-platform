package com.wqpthink.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulsarGatewayApplication.class, args);
    }

}
