package com.wqpthink.saas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarSaasApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulsarSaasApplication.class, args);
    }

}
