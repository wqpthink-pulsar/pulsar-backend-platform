package com.wqpthink.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulsarAuthApplication.class, args);
    }

}
