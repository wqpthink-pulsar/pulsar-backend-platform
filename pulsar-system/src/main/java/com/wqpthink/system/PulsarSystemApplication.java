package com.wqpthink.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulsarSystemApplication.class, args);
    }

}
